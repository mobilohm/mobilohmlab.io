// Get all the slide-arrow-prev and slide-arrow-next buttons
const prevButtons = document.querySelectorAll(".slide-arrow-prev");
const nextButtons = document.querySelectorAll(".slide-arrow-next");
var lightTheme = 'css/mobilohm-bulma-light.css';
var darkTheme = 'css/mobilohm-bulma-dark.css';

function switchTheme(theme) {
    // Get the existing theme link element
    var themeLink = document.getElementById('theme-link');

    // Create a new link element for the new theme
    var newThemeLink = document.createElement('link');
    newThemeLink.id = 'theme-link';
    newThemeLink.rel = 'stylesheet';
    newThemeLink.href = theme;

    // Replace the existing theme link with the new one
    themeLink.parentNode.replaceChild(newThemeLink, themeLink);
}

// Add event listeners to each pair of buttons
prevButtons.forEach((prevButton) => {
    prevButton.addEventListener("click", () => {
        const slidesContainer = prevButton.parentElement.querySelector(".slides-container");
        const slide = slidesContainer.querySelector(".slide");
        const slideWidth = slide.clientWidth;
        slidesContainer.scrollLeft -= slideWidth;
    });
});

nextButtons.forEach((nextButton) => {
    nextButton.addEventListener("click", () => {
        const slidesContainer = nextButton.parentElement.querySelector(".slides-container");
        const slide = slidesContainer.querySelector(".slide");
        const slideWidth = slide.clientWidth;
        slidesContainer.scrollLeft += slideWidth;
    });
});

function openModal(event) {
    event.stopPropagation();

    console.log('event: ', event);

    const ulElement = event.currentTarget;

    const modal = document.querySelector('.modal');
    const modalButtonDelete = modal.querySelector('.delete');
    // const modalCancelButton = modal.querySelector('.cancel');

    const modalButtonMobile = modal.querySelector('.mobile');
    const modalButtonDesktop = modal.querySelector('.desktop');
    const modalButtonSource = modal.querySelector('.source');
    const modalImage = document.createElement("img");
    modalImage.setAttribute('src', ulElement.dataset.logo);

    modalButtonMobile.addEventListener('click', () => {
        window.open(ulElement.dataset.mobile, '_blank');
    });

    modalButtonDesktop.addEventListener('click', () => {
        window.open(ulElement.dataset.desktop, '_blank');
    });

    modalButtonSource.addEventListener('click', () => {
        window.open(ulElement.dataset.source, '_blank');
    });

    const modalTitle = modal.querySelector('.modal-card-title');
    const modalBody = modal.querySelector('.modal-card-body');
    const modalDescription = modal.querySelector('.description');
    const modalLogo = modal.querySelector('.logo');

    modalLogo.innerHTML = '';
    modalLogo.appendChild(modalImage);

    modalTitle.textContent = ulElement.dataset.title;
    modalDescription.textContent = ulElement.dataset.description;

    modalButtonDelete.addEventListener('click', closeModal);
    // modalCancelButton.addEventListener('click', closeModal);

    modal.classList.add('is-active');
}

function closeModal() {
    var modal = document.querySelector('.modal');
    modal.classList.remove('is-active');
}
