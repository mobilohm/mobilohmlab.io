# MobilOhm

A collection of mobile-friendly apps

No bloat, no useless features, Free, Libre and Open-Source, syncs in the cloud 😎

![MobilOhm Apps](img/mobilohm-apps.png "MobilOhm Apps")

## All Apps
- [Dolores](https://mobilohm.gitlab.io/dolores/) (Will move to its own FQDN soon))
- [Todonna](https://todonna.gitlab.io/)
- Papiers (Ready soon)
- [Pétrolette](https://petrolette.space/)
